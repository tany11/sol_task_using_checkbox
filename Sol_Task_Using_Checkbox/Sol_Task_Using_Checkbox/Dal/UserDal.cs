﻿using Sol_Task_Using_Checkbox.Entity;
using Sol_Task_Using_Checkbox.Entity.Interface;
using Sol_Task_Using_Checkbox.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_Task_Using_Checkbox.Dal
{
    public class UserDal
    {
        #region Declaration
        private DataUserDataContext Dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            Dc = new DataUserDataContext();
        }


        #endregion

        public async Task<dynamic> UserData (UserEntity entityObj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() => {

                var getQuery =
                    Dc
                    ?.uspSetUser(
                        
                         entityObj?.UserId
                        , entityObj?.FirstName
                        , entityObj?.LastName
                        , entityObj?.Onsite
                        , ref status
                        , ref message
                    );

                return getQuery;

            });
        }

    }
}