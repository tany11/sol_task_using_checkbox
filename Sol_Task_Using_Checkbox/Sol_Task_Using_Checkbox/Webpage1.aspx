﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Webpage1.aspx.cs" Inherits="Sol_Task_Using_Checkbox.Webpage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

             <asp:ScriptManager ID="scriptManager" runat="server">

            </asp:ScriptManager>

            <asp:UpdatePanel ID="updatePanel" runat="server">
                <ContentTemplate>
                    <div style="float:left">
                        
                        <asp:TextBox ID="txtFirstName" runat="server" Text="FirstName"></asp:TextBox><br /><br>
                         <asp:TextBox ID="txtLastName" runat="server" Text="LastName"></asp:TextBox><br /><br>
                        <%--<asp:Label id="lblMessage" runat="server" EnableViewState="false"></asp:Label>--%>
                        <asp:CheckBox ID="chkOnsite" runat="server" Text="Onsite" Checked="false" AutoPostBack="true" /><br /><br>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_click" /><br /><br>
        </div>
                     </ContentTemplate>
            </asp:UpdatePanel>
            </div>
    </form>
</body>
</html>
