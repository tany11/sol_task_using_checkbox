﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Using_Checkbox.Entity.Interface
{
    interface IUserEntity
    {
        int UserId { get; set; }

        String FirstName { get; set; }

        String LastName { get; set; }

        bool Onsite { get; set; }
    }
}
