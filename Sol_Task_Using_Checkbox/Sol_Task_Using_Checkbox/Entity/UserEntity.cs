﻿using Sol_Task_Using_Checkbox.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Task_Using_Checkbox.Entity
{
    public class UserEntity: IUserEntity
    {
        public int UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public bool Onsite { get; set; }
    }
}